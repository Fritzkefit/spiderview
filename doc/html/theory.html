
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Theory: SPIDER &#8212; spiderview 1 documentation</title>
    <link rel="stylesheet" href="_static/alabaster.css" type="text/css" />
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
    <script type="text/javascript" id="documentation_options" data-url_root="./" src="_static/documentation_options.js"></script>
    <script type="text/javascript" src="_static/jquery.js"></script>
    <script type="text/javascript" src="_static/underscore.js"></script>
    <script type="text/javascript" src="_static/doctools.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="prev" title="Remarks on Using SpiderView" href="remarks.html" />
   
  <link rel="stylesheet" href="_static/custom.css" type="text/css" />
  
  
  <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9" />

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          

          <div class="body" role="main">
            
  <div class="section" id="theory-spider">
<span id="theory-ref"></span><h1>Theory: SPIDER<a class="headerlink" href="#theory-spider" title="Permalink to this headline">¶</a></h1>
<p>In this section I am going to briefly summarise the work that originally
introduced the SPIDER concept, <a class="reference internal" href="#iw99" id="id1">[IW99]</a>. The reconstruction algorithm in this
program was implemented as presented in that paper.</p>
<p><strong>S</strong>pectral <strong>P</strong>hase <strong>I</strong>nterferometry for <strong>D</strong>irect <strong>E</strong>lectric-field
<strong>R</strong>econstruction (<strong>SPIDER</strong>) is a technique to determine the time-dependent
electric-field of ultra-short laser pulses.</p>
<p>As the name suggests, the pulse to be characterised is interfered and measured
with a spectrometer. An analysis of the resulting interference pattern is able
to extract the sought after information, i.e. (spectral) phase and amplitude of
the pulse.</p>
<div class="section" id="reconstruction-algorithm">
<h2>Reconstruction Algorithm<a class="headerlink" href="#reconstruction-algorithm" title="Permalink to this headline">¶</a></h2>
<p>How can this be done? First, consider a pulse that is duplicated. One of the
two pulses is delayed by some time <span class="math notranslate nohighlight">\(\tau\)</span> and both are then focused
spatially onto one spot.  Assuming a Gaussian intensity profile in time and
space, one would measure the following signals in that place:</p>
<div class="align-center figure">
<img alt="image of fringes of interfered pulses" src="_images/fringes.png" />
</div>
<p>The (intensity) spectrum contains fringes spaced <span class="math notranslate nohighlight">\(2 \pi/\tau\)</span> apart since the spectrum
<span class="math notranslate nohighlight">\(\mathrm{D}(\omega)\)</span> is given by</p>
<div class="math notranslate nohighlight" id="equation-fringes">
<span class="eqno">(1)<a class="headerlink" href="#equation-fringes" title="Permalink to this equation">¶</a></span>\[\begin{split}&amp; \mathrm{D}(\omega) &amp; = &amp;\hphantom{2}|\mathrm{E}(\omega) + \mathrm{e}^{j\omega\tau}\mathrm{E}(\omega)|^2 = \\
&amp;                    &amp; = &amp; 2|\mathrm{E}(\omega)|^2 (1 + \cos(-\tau\omega))\end{split}\]</div>
<p>where <span class="math notranslate nohighlight">\(E(\omega)\)</span> is the pulse’s electric-field spectrum and the phase
factor <span class="math notranslate nohighlight">\(\mathrm{e}^{j\omega\tau}\)</span> corresponds to the time shift for one
of the duplicate pulses.</p>
<p>We can use the fringe spacing to encode the phase information in deviations
from the nominal spacing. These deviations occur after introducing spectral
shearing, i.e. a frequency shift <span class="math notranslate nohighlight">\(\Omega\)</span> between the two pulses in
addition to the time delay. The spectrum is then given by</p>
<div class="math notranslate nohighlight">
\[D(\omega) = |\mathrm{E}(\omega-\Omega)|^2 + |\mathrm{E}(\omega)|^2 +
2|\mathrm{E}(\omega-\Omega)\mathrm{E}(\omega)|\cos(\phi(\omega-\Omega) - \phi(\omega) - \tau\omega)\]</div>
<p>where <span class="math notranslate nohighlight">\(\phi\)</span> is the phase of the pulse. In contrast to <a class="reference internal" href="#equation-fringes">(1)</a>
where <span class="math notranslate nohighlight">\(\phi\)</span> cancels out, it is available from this spectrum in a
differential form</p>
<div class="math notranslate nohighlight">
\[\theta(\omega) = \phi(\omega-\Omega) -\phi(\omega).\]</div>
<p>Next we need to extract <span class="math notranslate nohighlight">\(\theta(\omega)\)</span>: The cosine can be
split into two AC-components, each with a phase factor <span class="math notranslate nohighlight">\(\mathrm{e}^{\pm
j\tau\omega}\)</span>:</p>
<div class="math notranslate nohighlight">
\[\begin{split}&amp;   &amp; 2|\mathrm{E}(\omega-\Omega)\mathrm{E}(\omega)|\cos(\theta(\omega) -
\tau\omega) =\\
&amp; = &amp; \hphantom{2}|\mathrm{E}(\omega-\Omega)\mathrm{E}(\omega)| \mathrm{e}^{+j\theta(\omega)}
\mathrm{e}^{+ j \tau\omega} +
|\mathrm{E}(\omega-\Omega)\mathrm{E}(\omega)| \mathrm{e}^{-j\theta(\omega)}
\mathrm{e}^{-j \tau\omega}\end{split}\]</div>
<p>In order to filter out one of the AC-components (and thus obtaining
<span class="math notranslate nohighlight">\(\theta(\omega)\)</span>), we Fourier transform the spectrum again. This time into a
pseudo time domain, where the phase factors <span class="math notranslate nohighlight">\(\mathrm{e}^{\pm
j\tau\omega}\)</span> shift the AC-components in (pseudo) time.  If the shift,
i.e. <span class="math notranslate nohighlight">\(\tau\)</span>, is big enough we obtain a three part signal:</p>
<div class="align-center figure">
<img alt="image of three-part signal" src="_images/three_part_signal.png" />
</div>
<p>Now we simply isolate one of the AC-components by straight-forward filtering
and do an inverse Fourier transform. The phase of the resulting AC-component is
equal to <span class="math notranslate nohighlight">\(\theta + \tau\omega\)</span> from which we subtract the linear
phase-correction term <span class="math notranslate nohighlight">\(\tau\omega\)</span> to obtain <span class="math notranslate nohighlight">\(\theta\)</span>.</p>
<p>If we assume that the spectral shear <span class="math notranslate nohighlight">\(\Omega\)</span> is small compared to the
bandwidth of our pulse, the differential phase is approximately proportional to
the derivative of the phase</p>
<div class="math notranslate nohighlight">
\[\theta(\omega) =
\phi(\omega-\Omega) - \phi(\omega) \approx \Omega \frac{\mathrm{d}\phi(\omega)}{\mathrm{d}\omega}.\]</div>
<p>After integration we are rewarded with the phase information!</p>
<div class="math notranslate nohighlight">
\[\phi(\omega) \approx \frac{1}{\Omega} \int \mathrm{d}\omega\theta\]</div>
<p>Now that we have got the phase of the pulse what about the amplitude? We could
either measure the so-called fundamental spectrum of the pulse in absence of
any manipulation, or we could use spectrum <a class="reference internal" href="#equation-fringes">(1)</a> (no spectral
shearing): Similar to the phase-information the amplitude-information is
contained within the DC-component of that spectrum. Therefore, we proceed as
above, Fourier transform, filter the DC-component, inverse Fourier transform
and be done since the obtained spectrum <cite>is</cite> the fundamental spectrum.</p>
</div>
<div class="section" id="practical-remarks">
<h2>Practical Remarks<a class="headerlink" href="#practical-remarks" title="Permalink to this headline">¶</a></h2>
<p>The theoretical concept assumes the time delay and the frequency shift as known
quantities. In Practice, however, calibration is necessary to obtain those
numbers (or the quantities that they are related to). Also the realisation
of the concept may influence the steps necessary for successful reconstruction.</p>
<p>A few things to point out with regard to this (software) implementation which
assumes a setup as described in <a class="reference internal" href="#iw99" id="id2">[IW99]</a>:</p>
<ul class="simple">
<li>Calibration<ul>
<li>The phase-correction term <span class="math notranslate nohighlight">\(\mathrm{e}^{j\tau\omega}\)</span> is obtained by
measuring the two pulses without spectral shearing (see
<a class="reference internal" href="#equation-fringes">(1)</a>). Similar to above, the phase of one AC-component is the
phase-correction. So there is no need to measure <span class="math notranslate nohighlight">\(\tau\)</span> directly.</li>
<li>The spectral shear <span class="math notranslate nohighlight">\(\Omega\)</span> can be obtained by measuring each of the
two pulses separately (in practice both are frequency shifted) and
determine <span class="math notranslate nohighlight">\(\Omega\)</span> as the difference of the center frequencies of
their spectra.</li>
</ul>
</li>
<li>Frequency Shifted Phase<ul>
<li>the phase needs to be frequency shifted to half of its center frequency
<cite>f0</cite> (i.e. its full original center frequency that got doubled).</li>
</ul>
</li>
</ul>
<p>For detailed information read the reference <a class="reference internal" href="#iw99" id="id3">[IW99]</a>.</p>
<table class="docutils citation" frame="void" id="iw99" rules="none">
<colgroup><col class="label" /><col /></colgroup>
<tbody valign="top">
<tr><td class="label">[IW99]</td><td><em>(<a class="fn-backref" href="#id1">1</a>, <a class="fn-backref" href="#id2">2</a>, <a class="fn-backref" href="#id3">3</a>)</em> Iaconis, Chris &amp; Walmsley, Ian. (1999). Self-referencing spectral
interferometry for measuring ultrashort optical pulses. Quantum
Electronics, IEEE Journal of. 35. 501 - 509. 10.1109/3.753654.</td></tr>
</tbody>
</table>
</div>
</div>


          </div>
          
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper"><div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="index.html">Documentation overview</a><ul>
      <li>Previous: <a href="remarks.html" title="previous chapter">Remarks on Using SpiderView</a></li>
  </ul></li>
</ul>
</div>
<div id="searchbox" style="display: none" role="search">
  <h3>Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" />
      <input type="submit" value="Go" />
      <input type="hidden" name="check_keywords" value="yes" />
      <input type="hidden" name="area" value="default" />
    </form>
    </div>
</div>
<script type="text/javascript">$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &copy;2018, Maximilian Geismann.
      
      |
      Powered by <a href="http://sphinx-doc.org/">Sphinx 1.7.9</a>
      &amp; <a href="https://github.com/bitprophet/alabaster">Alabaster 0.7.11</a>
      
      |
      <a href="_sources/theory.rst.txt"
          rel="nofollow">Page source</a>
    </div>

    

    
  </body>
</html>