.. _theory-ref:

================
 Theory: SPIDER
================

In this section I am going to briefly summarise the work that originally
introduced the SPIDER concept, [IW99]_. The reconstruction algorithm in this
program was implemented as presented in that paper.

**S**\pectral **P**\hase **I**\nterferometry for **D**\irect **E**\lectric-field
**R**\econstruction (**SPIDER**) is a technique to determine the time-dependent
electric-field of ultra-short laser pulses.

As the name suggests, the pulse to be characterised is interfered and measured
with a spectrometer. An analysis of the resulting interference pattern is able
to extract the sought after information, i.e. (spectral) phase and amplitude of
the pulse.

Reconstruction Algorithm
------------------------

How can this be done? First, consider a pulse that is duplicated. One of the
two pulses is delayed by some time :math:`\tau` and both are then focused
spatially onto one spot.  Assuming a Gaussian intensity profile in time and
space, one would measure the following signals in that place:

.. figure:: ./images/fringes.png
   :align: center
   :alt: image of fringes of interfered pulses
   :figclass: align-center

The (intensity) spectrum contains fringes spaced :math:`2 \pi/\tau` apart since the spectrum 
:math:`\mathrm{D}(\omega)` is given by

.. math::
   & \mathrm{D}(\omega) & = &\hphantom{2}|\mathrm{E}(\omega) + \mathrm{e}^{j\omega\tau}\mathrm{E}(\omega)|^2 = \\
   &                    & = & 2|\mathrm{E}(\omega)|^2 (1 + \cos(-\tau\omega))
   :label: fringes

where :math:`E(\omega)` is the pulse's electric-field spectrum and the phase
factor :math:`\mathrm{e}^{j\omega\tau}` corresponds to the time shift for one
of the duplicate pulses.

We can use the fringe spacing to encode the phase information in deviations
from the nominal spacing. These deviations occur after introducing spectral
shearing, i.e. a frequency shift :math:`\Omega` between the two pulses in
addition to the time delay. The spectrum is then given by

.. math::
   D(\omega) = |\mathrm{E}(\omega-\Omega)|^2 + |\mathrm{E}(\omega)|^2 + 
   2|\mathrm{E}(\omega-\Omega)\mathrm{E}(\omega)|\cos(\phi(\omega-\Omega) - \phi(\omega) - \tau\omega)

where :math:`\phi` is the phase of the pulse. In contrast to :eq:`fringes`
where :math:`\phi` cancels out, it is available from this spectrum in a
differential form 

.. math::
   \theta(\omega) = \phi(\omega-\Omega) -\phi(\omega).

Next we need to extract :math:`\theta(\omega)`: The cosine can be
split into two AC-components, each with a phase factor :math:`\mathrm{e}^{\pm
j\tau\omega}`:

.. math::
   &   & 2|\mathrm{E}(\omega-\Omega)\mathrm{E}(\omega)|\cos(\theta(\omega) - 
   \tau\omega) =\\
   & = & \hphantom{2}|\mathrm{E}(\omega-\Omega)\mathrm{E}(\omega)| \mathrm{e}^{+j\theta(\omega)}
   \mathrm{e}^{+ j \tau\omega} + 
   |\mathrm{E}(\omega-\Omega)\mathrm{E}(\omega)| \mathrm{e}^{-j\theta(\omega)}
   \mathrm{e}^{-j \tau\omega}

In order to filter out one of the AC-components (and thus obtaining
:math:`\theta(\omega)`), we Fourier transform the spectrum again. This time into a
pseudo time domain, where the phase factors :math:`\mathrm{e}^{\pm
j\tau\omega}` shift the AC-components in (pseudo) time.  If the shift,
i.e. :math:`\tau`, is big enough we obtain a three part signal:

.. figure:: ./images/three_part_signal.png
   :align: center
   :alt: image of three-part signal
   :figclass: align-center

Now we simply isolate one of the AC-components by straight-forward filtering
and do an inverse Fourier transform. The phase of the resulting AC-component is
equal to :math:`\theta + \tau\omega` from which we subtract the linear
phase-correction term :math:`\tau\omega` to obtain :math:`\theta`.

If we assume that the spectral shear :math:`\Omega` is small compared to the
bandwidth of our pulse, the differential phase is approximately proportional to
the derivative of the phase

.. math::
   \theta(\omega) =  
   \phi(\omega-\Omega) - \phi(\omega) \approx \Omega \frac{\mathrm{d}\phi(\omega)}{\mathrm{d}\omega}.

After integration we are rewarded with the phase information! 

.. math::
   \phi(\omega) \approx \frac{1}{\Omega} \int \mathrm{d}\omega\theta

Now that we have got the phase of the pulse what about the amplitude? We could
either measure the so-called fundamental spectrum of the pulse in absence of
any manipulation, or we could use spectrum :eq:`fringes` (no spectral
shearing): Similar to the phase-information the amplitude-information is
contained within the DC-component of that spectrum. Therefore, we proceed as
above, Fourier transform, filter the DC-component, inverse Fourier transform
and be done since the obtained spectrum `is` the fundamental spectrum.

Practical Remarks
-----------------

The theoretical concept assumes the time delay and the frequency shift as known
quantities. In Practice, however, calibration is necessary to obtain those
numbers (or the quantities that they are related to). Also the realisation
of the concept may influence the steps necessary for successful reconstruction.

A few things to point out with regard to this (software) implementation which
assumes a setup as described in [IW99]_:

* Calibration

  * The phase-correction term :math:`\mathrm{e}^{j\tau\omega}` is obtained by
    measuring the two pulses without spectral shearing (see
    :eq:`fringes`). Similar to above, the phase of one AC-component is the
    phase-correction. So there is no need to measure :math:`\tau` directly.

  * The spectral shear :math:`\Omega` can be obtained by measuring each of the
    two pulses separately (in practice both are frequency shifted) and
    determine :math:`\Omega` as the difference of the center frequencies of
    their spectra.

* Frequency Shifted Phase

  * the phase needs to be frequency shifted to half of its center frequency
    `f0` (i.e. its full original center frequency that got doubled).

For detailed information read the reference [IW99]_. 

.. [IW99] Iaconis, Chris & Walmsley, Ian. (1999). Self-referencing spectral
          interferometry for measuring ultrashort optical pulses. Quantum
          Electronics, IEEE Journal of. 35. 501 - 509. 10.1109/3.753654.

