=========
 Remarks on Using SpiderView
=========

Units and Files
---------------

Once you have chosen the unit (nm, m or Hz) on the first page of the
calibration wizard, all spectra read in by SpiderView are expected to contain
data over this unit. All spectra necessary for reconstruction will be transformed
into spectra over Hz before processing them. The results however, are converted
back to the wavelength-domain **with a hard-coded range of 500-1100nm**.

One exception is the reference plot data which is plotted as is. Since reference
plot occurs in the output plot window which displays the reconstructed pulse
over time, it should span the same range as the expected result.

Different sampling-widths (i.e. the delta between two frequencies/wavelengths)
are only allowed between the measurement+calibration spectrum and the
fundamental spectrum. Else the reconstruction will be flawed. The case of
differing dx between all three spectrum is easily handled but was not yet
implemented.

The implementation of units should be handled with some python package like `Unum
<http://home.scarlet.be/be052320/Unum.html>`_.

Calibration
-----------

The current calibration procedure does not process the separate red and blue
shifted spectra of the interfering pulses. You can choose an according file
path during calibration, but it will have no effect!  A further consequence is,
that the spectral shear :math:`\Omega` is calculated with the time delay
:math:`\tau` divided by the quadratic dispersion term :math:`\phi''`, see below.

Hard Coded Parameters
---------------------

Spider employs non-linear media. Especially a :math:`\chi^2` -crystal which will
double **approximately** double the centre-frequencies of the calibration and
the measurement-spectrum. Thus SpiderView shifts the spectra down by half the
(up-converted) centre-frequency. The easiest way to manipulate the shift is to
alter the f0-parameter in the output-tab and do a re-run.

Testing
-------

This software has not yet been extensively tested! On the GUI side, there might
be unexpected behaviour for unconventional (or even normal) user input.

The current implementation has been tested with real data and its results are
comparable to the ones of the existing LabView program `OnlineSpider.vi`.
