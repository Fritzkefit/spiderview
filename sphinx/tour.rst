============
 Quick Tour
============

In order to run the program, you need to call python on `spiderview.py` inside the project's spiderview directory (`path/to/spiderview/spiderview`):

.. highlight:: bash
   
::
   
   python spiderview.py

You should then be welcomed by the calibration tab as shown below.

.. figure:: ./images/calibration_tab.png
   :align: center
   :alt: alternate text
   :figclass: align-center

   The calibration tab: as in any other tab, the middles is occupied by the
   plotting area. On the left, the 'Acquire Data' button opens a wizard which
   will guide you through the necessary calibration steps. Below is an overview
   of the calibration steps and by clicking a step the associated spectrum is
   plotted. The currently selected spectrum can be filtered using the controls
   on the right. To start the actual calibration click on the
   'Calibrate'-button. Tau and Omega, the output values of calibration are then
   displayed on the left beneath the calibration parameters.

Basically the GUI is structured in three columns: on the left hand side you will
find main-controls e.g. to start the calibration process. In the middle is the
plot window that is used to visualise data. Beneath it are the standard pyplot
controls for moving, zooming or saving the plot. The right holds filter controls
in the calibration- and input-tab and displays pulse- and bandwidths in the
output tab.

The input tab should give an overview of the your input device(s) and execute
single measurements. Do not forget to set a decent filter setting before you
reconstruct!

.. figure:: ./images/input_tab.png
   :align: center
   :alt: alternate text
   :figclass: align-center

   Input tab: the measure button initiates a measurement through your input
   device. Alternatively, you can select a file via the 'Choose Path' button and
   enable it by checking the box to the left. The filter-options are consistent:
   the same filter is applied whether you choose to use a file or direct
   measurement (in the calibration-tab each spectrum is associated with one
   filter).

Finally, there is the output-tab:

.. figure:: ./images/output_tab.png
   :align: center
   :alt: alternate text
   :figclass: align-center

   Clicking the 'Run' button reconstructs the laser pulse. Afterwards you can
   inspect the results by choosing the desired quantity above the plotting
   window. A reference to compare the results with can be chosen by using the
   'Choose Path' button and checking the box to the left. Furthermore, you can
   choose to alter the parameters that were determined during calibration and
   repeat reconstruction by pressing the 'Run' button again.

After calibration and setting an appropriate input filter, press the 'Run'
button to fire up reconstruction.

That's it!
