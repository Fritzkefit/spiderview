==============
 Introduction
==============

SpiderView was written to replace an old LabView program that was used to
operate a SPIDER-device. It is tested on Python 2.7 and offers a qt4-based GUI.

SPIDER is a technique or device that enables the characterisation of ultra-short
laser pulses, i.e. it extracts the time-dependent electric-field of a very,
very short pulse (femtoseconds!). In order to do so, the pulse traverses a
contraption with non-linear elements and is eventually measured by a
spectrometer.  The emerging interference pattern at the spectrometer carries
the phase-information with which an algorithm can reproduce the
electric-field. More on that in the theory section, :doc:`theory`.

SpiderView itself aims to manage the calibration, data acquisition/procession and
visualisation of the results through an intuitive GUI.


