from matplotlib.pyplot import plot, show
from matplotlib import pyplot as plt
import numpy as np
from numpy.fft import fft, fftfreq, fftshift
from numpy import sqrt, exp, pi


def gauss(t):
    sigma = 20.
    return exp(-t**2/2/sigma**2)


w0 = 2*pi*10
t = np.array(range(-2**12, 2**12))
f = fftfreq(len(t))

t_idx = np.array(range(-2**7, 2**7))
pulse = list(sqrt(gauss(t_idx))*exp(1j*w0*t_idx))

DATA_POINTS = 16
signals = np.zeros(shape=(len(t), DATA_POINTS+1))
signals[:, 0] = t
spectra = np.zeros(shape=(len(f), DATA_POINTS+1))
spectra[:, 0] = fftshift(f)

pulses_i = 0
padded_t = 0
sum_t = pulse + pulse

padding = [0]*len(t_idx)
padding *= ((len(t)//2) // (len(t_idx)) - 1)
padded_t = padding + sum_t + padding

pulses_f = fft(fftshift(padded_t))
pulses_f /= max(abs(pulses_f))
pulses_i = abs(pulses_f)**2

plt.subplot(212)
plot(fftshift(f), fftshift(pulses_i))
# plt.grid()
plt.xlim([-0.02, 0.02])
plt.title('Spectrum')
ax = plt.gca()
ax.axes.get_xaxis().set_ticklabels([])
ax.axes.get_xaxis().set_ticks([])
ax.axes.get_yaxis().set_ticks([0,.5,1])

plt.subplot(211)
plot(t, padded_t)
# plt.grid()
plt.xlim([-1000, 1000])
plt.title('Time Domain')
ax = plt.gca()
ax.axes.get_xaxis().set_ticklabels([])
ax.axes.get_xaxis().set_ticks([])
ax.axes.get_yaxis().set_ticks([0,.5,1])
show()

