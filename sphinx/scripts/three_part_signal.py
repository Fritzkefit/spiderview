from matplotlib.pyplot import plot, show
from matplotlib import pyplot as plt
import numpy as np
from numpy.fft import fft, fftfreq, fftshift
from numpy import sqrt, exp, pi


def gauss(t):
    sigma = 20.
    return exp(-t**2/2/sigma**2)


w0 = 2*pi*.03
W = 2*pi*0.1
t = np.array(range(-2**12, 2**12))
f = fftfreq(len(t))

t_idx = np.array(range(-2**8, 2**8))
pulse = list(np.real(sqrt(gauss(t_idx))*exp(1j*w0*t_idx)))
pulse2 = list(np.real(sqrt(gauss(t_idx))*exp(1j*w0*t_idx+1j*W)))

pulses_i = 0
padded_t = 0
sum_t = pulse + pulse2

padding = [0]*len(t_idx)
padding *= ((len(t)//2) // (len(t_idx)) - 1)
padded_t = padding + sum_t + padding

pulses_f = fft(fftshift(padded_t))
pulses_f /= max(abs(pulses_f))
pulses_i = abs(pulses_f)**2

pulses_st = fft(pulses_i)
pulses_st = pulses_st/max(abs(pulses_st))

plot(t, fftshift(np.real(pulses_st)))
plt.xlim([-900, 900])
plt.title('Pseudo Time Domain')
ax = plt.gca()
ax.axes.get_xaxis().set_ticklabels([])
ax.axes.get_xaxis().set_ticks([])
ax.axes.get_yaxis().set_ticks([-1, -.5, 0, .5, 1])

plt.fill_between((t_idx+510),
                 0, exp(-((t_idx)/2/60)**4)/1.9,
                 color='0.8')
plt.fill_between((t_idx+510),
                 -exp(-((t_idx)/2/60)**4)/1.9, 0,
                 color='0.8')

ax.annotate('AC-component', xy=(330, 0.6))
show()
