reset

# set term gif animate delay 70
set term pngcairo

# set output 'fourier.gif'

set grid

set yrange [-0.05 : 1.05]
do for [i=2:17] {

   set output 'fourier_frame_'.(i+10).'.png'
   set multiplot
   set size 1,.5

   set xrange [-4096 : 4096]
   set origin .0, .5
   set title 'time domain'
   plot 'signals.txt' u 1:i w lines lw 2 notitle

   set xrange [-0.013 : 0.013]
   set origin .0, .0
   set title 'spectrum'
   plot 'spectra.txt' u 1:i w lines lw 2 notitle
   unset multiplot
}
set output
