from matplotlib.pyplot import plot, show
import numpy as np
from numpy.fft import fft, fftfreq, fftshift
from numpy import sqrt, exp, pi


def gauss(t):
    sigma = 20.
    return exp(-t**2/2/sigma**2)


w0 = 2*pi*10
t = np.array(range(-2**12, 2**12))
f = fftfreq(len(t))

t_idx = np.array(range(-2**7, 2**7))
pulse = list(sqrt(gauss(t_idx))*exp(1j*w0*t_idx))

DATA_POINTS = 16
signals = np.zeros(shape=(len(t), DATA_POINTS+1))
signals[:, 0] = t
spectra = np.zeros(shape=(len(f), DATA_POINTS+1))
spectra[:, 0] = fftshift(f)

pulses_i = 0
padded_t = 0
sum_t = []
for i in range(DATA_POINTS):

    sum_t = sum_t + pulse

    if i != 0:
        sum_t = pulse + sum_t

    padding = ([0]*(len(t_idx)//2))
    padding *= ((len(t)//2)//(len(t_idx)//2) - 1 - 2*i)
    padded_t = padding + sum_t + padding

    signals[:, i+1] = np.array(np.real(padded_t))
    
    pulses_f = fft(fftshift(padded_t))
    pulses_f /= max(abs(pulses_f))
    pulses_i = abs(pulses_f)**2

    spectra[:, i+1] = fftshift(pulses_i)

# show(plot(f, pulses_i))
# show(plot(t, padded_t))
show(plot(t, signals[:,1]))
with open('signals.txt', 'wb') as file:
    np.savetxt(file, signals)

with open('spectra.txt', 'wb') as file:
    np.savetxt(file, spectra)
