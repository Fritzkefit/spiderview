.. spiderview documentation master file, created by
   sphinx-quickstart on Tue Feb  6 16:48:39 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SpiderView's rudimentary documentation!
==================================================

This documentation should enable you continue my work. Keep in mind that this
documentation, as well as the program itself, is a rough piece of craft; neither
polished **nor** finished!

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro
   tour
   remarks
   theory
   structure



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
