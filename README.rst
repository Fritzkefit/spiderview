spiderView
==========

This application reads, processes and visualizes data of a measurement-appartus
that deploys a method called *Spectral Phase
Interferometry for Direct Electric Field Reconstruction* (**SPIDER**) [IW99]_
for characterization of ultra short laser pulses.

This project was created as part of a course at the institute of 
photonics, TU Wien.

Run the program with from the directory `spiderview` as:
.. highlight:: bash
   
::
   
   python spiderview.py

As test data for a quick demonstration you can use
`testdata/calib_500fs/calib_spec_500fs.dat` as the calibration-spectrum,
`/testdata/fundamental.dat` as the fundamental-spectrum and any of starting with
`m` in `/testdata/comp_frog_300fs`, e.g. `/testdata/comp_frog_300fs/m1.dat`
(this data is read in by default when you press the `measure` button in the
input-tab.) Do not be confused by the `300fs` in the path, the data matches.

----

.. [IW99] C. Iaconis and I. A. Walmsley, 
          "Self-referencing spectral interferometry for measuring ultrashort optical pulses," in IEEE Journal of Quantum Electronics, vol. 35, no. 4, pp. 501-509, Apr 1999.
          doi: 10.1109/3.753654
