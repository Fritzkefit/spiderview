"""
Input module for the spectrometer *Ocean Optics 2000*.

The code in this module drives and controls the input device.
"""

from common import convert_to_frequency_spectrum
import numpy as np
import Spectrum


class OceanOptics2000(object):

    def __init__(self):
        pass

    def measure(self, unit):
        x, y = np.loadtxt('testdata/comp_frog_300fs/m4.dat', unpack=True)

        return Spectrum.Spectrum([x, y], unit)
