# -*- coding: utf-8 -*-
"""This module is USELESS... it need to be replaced!

As of now, this module only stores redundant data from the spiderview-object and
faintly adjusts the return of the algorithm module. It should become the
\"outputTab-object\" which should be implemented in the future and should
initialise the GUI and store data of the output-tab.
"""

import numpy as np

from common import c0
from common import convert_to_frequency_spectrum
import algorithm


class Reconstruction(object):

    def __init__(self, f0=None, tau=None, omega=None, dispersion=None):

        self.f0 = f0
        self.tau = tau
        self.omega = omega
        self.dispersion = dispersion
        self.spec_limits = None
        self.spec_limits_idx = None

    def process(self, signal, fun_spec, calib_spec):
        if(self.tau is None or self.omega is None):
            raise ValueError("Parameters tau or omega are not defined!"
                             "Please do the calibration first.")

        result = algorithm.reconstruction(
            self.tau, self.omega, self.f0/2.,
            self.spec_limits, self.spec_limits_idx,
            signal.spec_Hz, calib_spec.spec_Hz, fun_spec.spec_Hz)

        result = [result[0], result[1], np.abs(result[1])**2,
                  result[2], result[3], np.abs(result[3])**2]

        return result
