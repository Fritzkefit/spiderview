"""
Module gathering common classes and functions needed throughout spiderview.
"""

from PyQt4 import QtGui
import numpy as np
import scipy


c0 = 299792458  # speed of light


def to_ascending_order(data):

    """
    Converts a signal from standard-order (freqs for fft) to normal \"ascending\"
    order. If it already is in normal order, do nothing.
    """

    # heuristic!! determine if signal is in fft standard-order
    if data[0][0] > data[0][-1] and data[0][-1] < 0:
        return [np.fft.fftshift(data[0]), np.fft.fftshift(data[1])]
    else:
        return [data[0], data[1]]


def smart_limits(data, margin=0.05, threshold=0.05, maxleft=None, minright=None):

    """
    Finds the optimal plotting range for the x-axis.

    Often a spectrum contains mainly zeros and only narrow band is non-zero.
    This function spares the user of manually adjusting the x-axis to the
    interesting range.

    :param: xy-data in standard order (*not* FFT order)
    :param: threshold as percentage of maximum value
    :param: assures left filter edge is always visible
    :param: assures right filter edge is always visible
    :return: tupel x-axis indices encapsulating range"""

    threshold *= np.max(np.abs(data[1]))
    npts = len(data[1])
    left = data[0][0]
    for i in range(npts):
        if np.abs(data[1][i]) > threshold:
            idx = i-int(npts*margin)
            if idx >= 0:
                left = data[0][idx]
            else:
                left = data[0][i]
            break

    right = data[0][-1]
    for i in range(npts):
        if np.abs(data[1][-i-1]) > threshold:
            idx = -(i+1)+int(npts*margin)
            if idx < 0:
                right = data[0][idx]
            else:
                right = data[0][-i-1]
            break

    if maxleft is not None and left > maxleft:
        left = maxleft
    if minright is not None and right < minright:
        right = minright

    return (left, right)


def get_fwhm(data):

    """
    Calculates the indices of the fwhm and the fwhm itself of data.
    """

    data = to_ascending_order(data)
    limits = smart_limits(data, margin=0., threshold=.5)

    return limits[1] - limits[0]


def convert_to_frequency_spectrum(spectrum, df=None, normalise=False):
    """
    Converts a spectrum over wavelength into a spectrum over frequency.

    :param: Uniformly sampled wavelength spectrum
    :param: Difference of two sample-points in interpolated spectrum
    :param: Determines by what factor the number sample points de/increases
    :return: Jacobian-transformed spectrum uniformly spaced over frequency
    """

    if spectrum is None:
        return None

    spectrum[0] = np.flipud(c0/spectrum[0])
    spectrum[1] = np.flipud(spectrum[1])*c0/spectrum[0]**2

    factor = 2.  # for extending frequency-range to zero with additional samples
    if df is None:
        if np.isinf(spectrum[0][-1]) or np.isnan(spectrum[0][-1]):
            new_df = (spectrum[0][-2])/len(spectrum[0])/factor
        else:
            new_df = spectrum[0][-1]/len(spectrum[0])/factor
    else:
        new_df = df

    new_f = [i*new_df for i in range(int(factor)*len(spectrum[0]))]
    spectrum[1] = np.interp(new_f, spectrum[0], spectrum[1], left=0., right=0.)
    spectrum[0] = np.array(new_f)

    if normalise:
        spectrum[1] /= max(abs(spectrum[1]))

    return spectrum


def convert_to_wavelength_spectrum(spec, df=None):
    """Converts a spectrum over frequency into a spectrum over wavelength.

    :param: Uniformly sampled frequency spectrum
    :return: Jacobian-transformed spectrum uniformly spaced over wavelength

    This function was copied '1:1' from the original OnlineSpider.vi-program. It
    hardcodes the wavelength range of the spectrum which could be handled better
    """

    # 1:1 from OnlineSpider
    l_max = 1.1e-6
    l_min = 5e-7

    nsmpls = len(spec[0])

    l = np.linspace(l_min, l_max, nsmpls)
    f = c0/l

    signal = scipy.interpolate.interp1d(spec[0], spec[1], fill_value='extrapolate', kind='cubic')

    I = f**2/c0 * signal(f)
    I /= max(abs(I))

    return [l, I]

def hypergaussian(x, order, fwhm, offset=0.):
    """Hypergaussian intened as a bandpass-filter"""
    return np.exp(-np.log(2)*(2/fwhm*(x-offset))**(2*order))
