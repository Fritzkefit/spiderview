from common import smart_limits, get_fwhm, convert_to_wavelength_spectrum
from common import convert_to_frequency_spectrum, hypergaussian

import numpy as np
import scipy.signal


class SpectrumFilter(object):
    """Simple bandpass-filter

    Uses a hypergaussian to filter a given spectrum.

    THIS IS A MESS... must redo whole spectrum+filter concept!
    """

    def __init__(self, npts):

        self.npts = npts
        self.x = np.array([i for i in range(int(-(npts-1)/2.), int(npts/2.)+1)])
        self.fwhm = 0.3*npts
        self.order = 20
        self.offset = 0 # filter will be in middle of data
        self.background_level = 0
        self.savitzky_golay = False

    def _update(self, npts):
        self.npts = npts
        self.x = np.array([i for i in range(int(-(self.npts-1)/2.), int(self.npts/2.)+1)])
        self.fwhm = 0.3*self.npts

    @property
    def filter_function(self):
        if self.npts == 0:
            return None
        return [self.x, hypergaussian(self.x, self.order, self.fwhm, self.offset)]

    def filter(self, spec):
        if self.npts == 0:
            self._update(len(spec[0]))

        ret = [spec[0], spec[1]]
        if self.savitzky_golay:
            ret[1] = scipy.signal.savgol_filter(spec[1], 21, 5)
        window = hypergaussian(self.x, self.order, self.fwhm, self.offset)
        return [ret[0], (ret[1] - self.background_level)*window]


class Spectrum(object):

    def __init__(self, spec=None, unit=None, filter=None):

        self.spec = spec
        self.unit = unit
        if filter is None:
            self.filter = SpectrumFilter(0)
        else:
            self.filter = filter

    @property
    def spec(self):
        return self._spec

    @spec.setter
    def spec(self, spec):

        if spec is not None:
            self._spec = spec
            self.filter = SpectrumFilter(len(spec[0]))
        else:
            self._spec = None
            self.filter = None

    @property
    def spec_filtered(self):

        if self.spec is not None:
            spec = self.filter.filter(self.spec)
            return [spec[0], spec[1]/max(abs(spec[1]))]
        else:
            return None

    @property
    def spec_Hz(self):

        spec = self.spec_filtered
        if self.unit == "nm" and spec is not None:
            spec = [spec[0]*1e-9, spec[1]]
            return convert_to_frequency_spectrum(spec, normalise=True)
        elif self.unit == "m":
            return convert_to_frequency_spectrum(spec, normalise=True)
        elif self.unit == "Hz":
            return [spec[0], spec[1]/max(abs(spec[1]))]
        else:
            return None
        # raise ValueError("Spectrum is not associated with any unit!")
