# -*- coding: utf-8 -*-
"""
Main-module containing the GUI and managing data between input,
signal-processing module.

This module interfaces with the signal-processing and input modules.
It also loads the GUI and handles its events.
"""

from common import smart_limits, get_fwhm, convert_to_wavelength_spectrum
from common import to_ascending_order
import Calibration
import Reconstruction
import Spectrum
from OceanOptics2000 import OceanOptics2000

import time
import sys

from PyQt4 import QtGui, QtCore, uic
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
from matplotlib import pyplot as plt
import numpy as np
import random


version = "0.1"


class MyMplCanvas(FigureCanvas):
# THIS CLASS SHOULD BE MOVED INTO A SEPARATE FILE
    """
    A Matplotlib-canvas used for plotting data

    It is embedded into the GUI.
    """

    def __init__(self, parent=None, width=2, height=4, dpi=50):

        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)
        self.compute_initial_figure()
        FigureCanvas.__init__(self, fig)
        self.setParent(parent)
        self.axes.tick_params(axis='both', which='major', size=8)

        self.reference_plot = False
        self.reference_plot_path = None
        self.current_plot_data = None

    def compute_initial_figure(self):
        self.axes.annotate("SPIDERVIEW", (.35, .5))

    def clear(self):
        self.axes.clear()
        self.draw()

    def plot(self, args, reference=None, autoscale=True, maxleft=None, minright=None):
        """Plots a spectrum (or anything else if you want)."""
        self.axes.clear()

        self.current_plot_data = args

        if reference is not None:
            self.axes.plot(1e-9*reference[0], reference[1], color='#B6B6B4')

        if args is not None:
            self.axes.plot(*args)
            if autoscale:
                self.axes.set_xlim(smart_limits(args, maxleft=maxleft, minright=minright))
        self.axes.autoscale(axis='y')

        self.draw()

    def plot_filter(self, filter, autoscale=True):
        """Plots the applied filter in red."""
        filter_function = filter.filter_function
        self.axes.plot(self.current_plot_data[0], filter_function[1], color='r')
        if autoscale:
            self.axes.set_xlim(
                smart_limits(
                    [self.current_plot_data[0], filter_function[1]]))
        # self.axes.plot(self.current_plot_data)
        self.draw()

    def toggle_reference_plot(self):
        self.reference_plot = not self.reference_plot

        if self.reference_plot:
            self.plot(self.current_plot_data, self.reference_plot_data)
        else:
            self.plot(self.current_plot_data)

    @property
    def reference_plot_path(self):
        return self.reference_plot_path

    @reference_plot_path.setter
    def reference_plot_path(self, path):

        if path is not None:
            x, y = np.loadtxt(path, unpack=True, dtype=complex)
            self.reference_plot_data = [x, abs(y/max(abs(y)))]


# for floating-point-debug purposes
def err_handler(type, flag):
    print("Floating point error (%s), with flag %s" % (type, flag))


class SpiderView(QtGui.QMainWindow):

    def __init__(self):

        # DEBUG
        np.seterrcall(err_handler)
        np.seterr(all='call')

        self.result = None
        self.current_plot = None
        self.input_filter = None

        self.tau = None
        self.omega = None
        self.peaks = None
        self.peaks_idx = None

        GDD = 220e-27  # fs²/mm
        glass_length = 0.1  # m

        # load and initialize modules
        # TODO: deprecate these three by using new
        # objects, one for each tab. Each tab-object should hold current data
        # and share data (e.g. parameters) via SpiderView. Also, they should be
        # responsible for initialising their part of the GUI
        self.spectrometer = OceanOptics2000()
        self.calibration = Calibration.Calibration(self.spectrometer, GDD, glass_length)
        self.reconstruction = Reconstruction.Reconstruction()

        # initialize GUI
        QtGui.QMainWindow.__init__(self)
        uic.loadUi("gui/spiderview.ui", self)
        self.show()  # show the GUI

        # Calibration
        self.pushButton_acquire_data.clicked.connect(self.acquire_data)
        self.pushButton_calibrate.clicked.connect(self.calibration.calibrate)

        # calibration-control left side
        self.radioButton_calib_step_calib_spec.clicked.connect(
            self.change_calib_step)
        self.radioButton_calib_step_fun_spec.clicked.connect(
            self.change_calib_step)
        self.radioButton_calib_step_omega1_spec.clicked.connect(
            self.change_calib_step)
        self.radioButton_calib_step_omega2_spec.clicked.connect(
            self.change_calib_step)

        # default values
        self.spinBox_GDD.setValue(GDD/1e-27)  # fs^2/mm
        self.spinBox_glass_length.setValue(0.1) # m
        self.spinBox_GDD.valueChanged.connect(
            self.update_GDD)
        self.spinBox_glass_length.valueChanged.connect(
            self.update_glass_length)

        # calibration plotFrame
        self.current_calibration_plot = None

        self.canvas_calibration = MyMplCanvas(
            self.plotFrame_calibration, width=5, height=4, dpi=100)
        self.toolbar = NavigationToolbar(self.canvas_calibration, self)
        self.plotLayout_calibration.addWidget(self.canvas_calibration)
        self.plotLayout_calibration.addWidget(self.toolbar)

        # calibration-control right side
        self.checkBox_calibration_show_original.clicked.connect(
            self.toggle_original_plot)
        self.spinBox_calibration_fwhm.valueChanged.connect(
            self.update_fwhm)
        self.spinBox_calibration_offset.valueChanged.connect(
            self.update_offset)
        self.spinBox_calibration_background_level.valueChanged.connect(
            self.update_background_level)
        self.spinBox_calibration_background_level.setSingleStep(0.01)
        self.checkBox_calibration_SV.stateChanged.connect(
            self.toggle_SV_filtering)

        # input-control left side
        self.pushButton_input_file.clicked.connect(self.input_choose_file)
        self.checkBox_input_file.setEnabled(False)
        self.checkBox_input_file.stateChanged.connect(self.input_toogle_file)
        self.lineEdit_input_file.setEnabled(False)
        self.pushButton_measure.clicked.connect(self.measure)

        # input-control plotFrame
        self.canvas_input = MyMplCanvas(self.plotFrame_input, width=5,
                                        height=4, dpi=100)
        self.toolbar = NavigationToolbar(self.canvas_input, self)
        self.plotLayout_input.addWidget(self.canvas_input)
        self.plotLayout_input.addWidget(self.toolbar)

        # input-control right side
        self.checkBox_input_show_original.clicked.connect(
            self.toggle_original_plot)
        self.spinBox_input_fwhm.valueChanged.connect(
            self.update_fwhm)
        self.spinBox_input_offset.valueChanged.connect(
            self.update_offset)
        self.spinBox_input_background_level.valueChanged.connect(
            self.update_background_level)
        self.spinBox_input_background_level.setSingleStep(0.01)
        self.checkBox_input_SV.stateChanged.connect(
            self.toggle_SV_filtering)

        # output plotFrame
        self.canvas_output = MyMplCanvas(self.plotFrame_output, width=5,
                                         height=4, dpi=100)
        self.toolbar = NavigationToolbar(self.canvas_output, self)
        self.plotLayout_output.addWidget(self.canvas_output)
        self.plotLayout_output.addWidget(self.toolbar)

        # output-control button groupbox
        self.spinBox_f0.valueChanged.connect(self.update_f0)
        self.spinBox_tau.valueChanged.connect(self.update_tau)
        self.spinBox_omega.valueChanged.connect(self.update_omega)

        # connect pushButton and checkBox signals
        self.pushButton_run.clicked.connect(self.run)
        self.pushButton_calibrate.clicked.connect(self.calibrate)
        self.pushButton_reference_plot_choose_path.clicked.connect(
            self.reference_plot_choose_path)
        self.checkBox_continuous.clicked.connect(self.run_continuous)
        self.checkBox_reference_plot.setEnabled(False)
        self.checkBox_reference_plot.clicked.connect(
            self.canvas_output.toggle_reference_plot)

        self.radioButton_Et.clicked.connect(self._plot_Et)
        self.radioButton_Et.setChecked(True)
        self.radioButton_It.clicked.connect(self._plot_It)
        self.radioButton_argIl.clicked.connect(self._plot_argIl)
        self.radioButton_absIl.clicked.connect(self._plot_absIl)

        # avoid manual calibration during testing
        # self._auto_calibrate()

    def _auto_calibrate(self):
        """
        Spares the use from clicking through the calibration process.

        This function was written for quickly testing the program with actual
        data. It loads spectra from hardcoded paths and sets appropriate filters.
        """
        # return
        self.calibration.unit = "nm"
        x, y = np.loadtxt(
            "/home/fritzkefit/Documents/uni/12.semester/PhV/spiderview/"
            "spiderview/testdata/calib_500fs/calib_spec_500fs.dat", unpack=True)
        self.calibration.calib_spec = Spectrum.Spectrum([x,y], "nm", Spectrum.SpectrumFilter(len(x)))
        self.calibration.calib_spec.filter.offset = 40
        self.calibration.calib_spec.filter.fwhm = 350
        self.calibration.calib_spec.filter.background_level = 0.125

        x, y = np.loadtxt(
            "/home/fritzkefit/Documents/uni/12.semester/PhV/spiderview/"
            "spiderview/testdata/fundamental.dat", unpack=True)
        # self.calibration.fun_spec = Spectrum.Spectrum([x,y], "nm", Spectrum.SpectrumFilter(len(x)))
        self.calibration.fun_spec = Spectrum.Spectrum()
        self.calibration.fun_spec.filter.offset = -300
        self.calibration.fun_spec.filter.fwhm = 300
        self.calibration.fun_spec.filter.background_level = 0.045

        self.calibration.omega1_spec = Spectrum.Spectrum()
        self.calibration.omega2_spec = Spectrum.Spectrum()

        self.enable_calib_step_buttons()

    def acquire_data(self):
        self.calibration.acquire_data()
        self.enable_calib_step_buttons()

    def enable_calib_step_buttons(self):

        if self.calibration.calib_spec.spec is not None:
            self.radioButton_calib_step_calib_spec.setEnabled(True)

        if self.calibration.fun_spec.spec is not None:
            self.radioButton_calib_step_fun_spec.setEnabled(True)

        if self.calibration.omega2_spec.spec is not None:
            self.radioButton_calib_step_omega1_spec.setEnabled(True)

        if self.calibration.omega1_spec.spec is not None:
            self.radioButton_calib_step_omega2_spec.setEnabled(True)

    def run(self):

        """
        Initiate the resconstruction process:
        measure -> reconstruct -> display results
        """

        if self.checkBox_input_file.isChecked():
            signal = self.current_input_spec
        else:
            signal = self.spectrometer.measure(self.calibration.unit)
            self.set_input_filter(signal)
            self.current_input_spec = signal
        self.plot_input()

        self.result = self.reconstruction.process(
            signal,
            self.calibration.fun_spec,
            self.calibration.calib_spec)
        # self.canvas_output.plot(to_ascending_order(self.result[:2]))
        self._plot_Et()

        tmp = get_fwhm([self.result[0], self.result[2]])
        self.label_pulse_width.setText("{:1.4f} fs".format(tmp/1e-15))

        tmp = convert_to_wavelength_spectrum(
            [self.result[3], self.result[5]])
        tmp = get_fwhm(tmp)
        self.label_band_width.setText("{:1.4f} nm".format(tmp/1e-9))

    def run_continuous(self):
        raise NotImplementedException("Hopefully in the future")

    # The next two functions should be a method of some sort file-handler object
    def reference_plot_choose_path(self):
        file_dialog = QtGui.QFileDialog()
        self.canvas_output.reference_plot_path = file_dialog.getOpenFileName(
            None, 'Choose file',
            "", "Text, DAT or CSV files (*.txt *.dat *.csv)")
        self.checkBox_reference_plot.setEnabled(True)

    def input_choose_file(self):
        file_dialog = QtGui.QFileDialog()
        self.lineEdit_input_file.setText(
            file_dialog.getOpenFileName(
            None, 'Choose file',
            "", "Text, DAT or CSV files (*.txt *.dat *.csv)"))
        self.checkBox_input_file.setEnabled(True)
        self.checkBox_input_file.setChecked(True)
        self.lineEdit_input_file.setEnabled(True)

    def input_toogle_file(self):
        """Displays the data of the currently selected input-file"""
        if self.checkBox_input_file.isChecked():
            x, y = np.loadtxt(self.lineEdit_input_file.text(), unpack=True)
            signal = Spectrum.Spectrum([x, y], self.calibration.unit, self.input_filter)
            self.set_input_filter(signal)
            self.set_filter_params(signal.filter)

            self.current_input_spec = signal
            self.plot_input()
        else:
            self.canvas_input.clear()

    def calibrate(self):
        result = self.calibration.calibrate()

        self.reconstruction.f0 = np.mean(result[2])  # TODO: check, improve
        self.reconstruction.tau = result[0]
        self.reconstruction.omega = result[1]
        self.reconstruction.dispersion = self.calibration.dispersion
        self.reconstruction.spec_limits = result[2]
        self.reconstruction.spec_limits_idx = result[3]

        self.label_tau.setText("{:1.0f} fs".format(result[0]/1e-15))
        self.label_omega.setText("{:1.2f} THz".format(result[1]/1e12))

        self.spinBox_f0.setValue(np.mean(result[2])/1e12)  # THz
        self.spinBox_tau.setValue(result[0]/1e-15)  # fs
        self.spinBox_omega.setValue(result[1]/1e12)  # THz

    def change_calib_step(self):
        if self.radioButton_calib_step_calib_spec.isChecked():
            self.current_calib_spec = self.calibration.calib_spec
            self.set_filter_params(self.current_calib_spec.filter)

        if self.radioButton_calib_step_fun_spec.isChecked():
            self.current_calib_spec = self.calibration.fun_spec
            self.set_filter_params(self.current_calib_spec.filter)

        if self.radioButton_calib_step_omega1_spec.isChecked():
            self.current_calib_spec = self.calibration.omega1_spec
            self.set_filter_params(self.current_calib_spec.filter)

        if self.radioButton_calib_step_omega2_spec.isChecked():
            self.current_calib_spec = self.calibration.omega2_spec
            self.set_filter_params(self.current_calib_spec.filter)
        self.plot_calib_step()

    def plot_calib_step(self):
        if self.radioButton_calib_step_calib_spec.isChecked():
            self.canvas_calibration.plot(self.calibration.calib_spec.spec_filtered)
            self.canvas_calibration.plot_filter(self.calibration.calib_spec.filter)
            self.current_calib_spec = self.calibration.calib_spec
            self.set_filter_params(self.current_calib_spec.filter)

        if self.radioButton_calib_step_fun_spec.isChecked():
            self.canvas_calibration.plot(self.calibration.fun_spec.spec_filtered)
            self.canvas_calibration.plot_filter(self.calibration.fun_spec.filter)
            self.current_calib_spec = self.calibration.fun_spec

        if self.radioButton_calib_step_omega1_spec.isChecked():
            self.canvas_calibration.plot(self.calibration.omega1_spec.spec_filtered)
            self.canvas_calibration.plot_filter(self.calibration.omega1_spec.filter)
            self.current_calib_spec = self.calibration.omega1_spec

        if self.radioButton_calib_step_omega2_spec.isChecked():
            self.canvas_calibration.plot(self.calibration.omega2_spec.spec_filtered)
            self.canvas_calibration.plot_filter(self.calibration.omega2_spec.filter)
            self.current_calib_spec = self.calibration.omega2_spec

    def plot_input(self):
        self.canvas_input.plot(self.current_input_spec.spec_filtered)
        self.canvas_input.plot_filter(self.current_input_spec.filter)

    def measure(self):
        self.checkBox_input_file.setChecked(False)
        signal = self.spectrometer.measure(self.calibration.unit)

        self.set_input_filter(signal)

        self.current_input_spec = signal
        self.set_filter_params(signal.filter)
        self.plot_input()
        # self.canvas_input.plot(signal.spec_filtered)

    def set_input_filter(self, signal):
        """Creates or a new input-filter or reuses the old one if applicable."""
        npts = len(signal.spec[0])
        if self.input_filter is not None and \
           self.input_filter.npts == npts:
                signal.filter = self.input_filter
        else:
            self.input_filter = Spectrum.SpectrumFilter(npts)
            signal.filter = self.input_filter

    def _plot_Et(self):
        self.canvas_output.plot(to_ascending_order(
            [self.result[0], np.real(self.result[1])]), autoscale=False)

    def _plot_It(self):
        self.canvas_output.plot(
            to_ascending_order([self.result[0], self.result[2]]), autoscale=False)

    def _plot_argIl(self):
        tmp = convert_to_wavelength_spectrum(self.result[3:5])
        self.canvas_output.plot([tmp[0], np.unwrap(np.angle(tmp[1]))],
                                autoscale=False)

    def _plot_absIl(self):
        tmp = convert_to_wavelength_spectrum(self.result[3:5])
        self.canvas_output.plot([tmp[0], np.abs(tmp[1])/np.max(np.abs(tmp[1]))],
                                autoscale=False)

    def toggle_original_plot(self):
        raise NotImplementedError("hopefully in the future")

    def update_f0(self):
        self.reconstruction.f0 = self.spinBox_f0.value()*1e12

    def update_tau(self):
        self.reconstruction.tau = self.spinBox_tau.value()*1e-15

    def update_omega(self):
        self.reconstruction.omega = self.spinBox_omega.value()*1e12

    def update_GDD(self):
        self.calibration.GDD = self.spinBox_GDD.value()*1e-27

    def update_glass_length(self):
        self.calibration.glass_length = self.spinBox_glass_length.value()

    # Again bad code-structure: this could be handled much easier in separate
    # tab-objects as mentioned above
    def set_filter_params(self, filter):
        if self.tabWidget.currentIndex() == 0:
            self.spinBox_calibration_offset.setValue(filter.offset)
            self.spinBox_calibration_fwhm.setValue(filter.fwhm)
            self.spinBox_calibration_background_level.setValue(filter.background_level)
            self.checkBox_calibration_SV.setChecked(filter.savitzky_golay)
        if self.tabWidget.currentIndex() == 1:
            self.spinBox_input_offset.setValue(filter.offset)
            self.spinBox_input_fwhm.setValue(filter.fwhm)
            self.spinBox_input_background_level.setValue(filter.background_level)
            self.checkBox_input_SV.setChecked(filter.savitzky_golay)

    def update_offset(self, val):
        if self.tabWidget.currentIndex() == 0:
            self.current_calib_spec.filter.offset = val
            self.plot_calib_step()
        elif self.tabWidget.currentIndex() == 1:
            self.input_filter.offset = val
            self.plot_input()

    def update_fwhm(self, val):
        if self.tabWidget.currentIndex() == 0:
            self.current_calib_spec.filter.fwhm = val
            self.plot_calib_step()
        elif self.tabWidget.currentIndex() == 1:
            self.input_filter.fwhm = val
            self.plot_input()

    def update_background_level(self, val):
        if self.tabWidget.currentIndex() == 0:
            self.current_calib_spec.filter.background_level = val
            self.plot_calib_step()
        elif self.tabWidget.currentIndex() == 1:
            self.input_filter.background_level = val
            self.plot_input()

    def toggle_SV_filtering(self):
        if self.tabWidget.currentIndex() == 0:
            checked = self.checkBox_calibration_SV.isChecked()
            self.current_calib_spec.filter.savitzky_golay = checked
            self.plot_calib_step()
        elif self.tabWidget.currentIndex() == 1:
            checked = self.checkBox_input_SV.isChecked()
            self.input_filter.savitzky_golay = checked
            self.plot_input()
        else:
            raise ValueError("No tab selected?!")

if(__name__ == "__main__"):

    app = QtGui.QApplication(sys.argv)
    window = SpiderView()

    sys.exit(app.exec_())
