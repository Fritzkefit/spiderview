"""Manages the calibraiton wizard and associated parameters+values

This module, in contrast to Reconstruction.py, is a better (yet not good)
example of how spiderview should be structure in the future: It handles the GUI
of the calibration-wizard and manages calibration parameters and results.
However, there should be another object taking care of the calibration-tab (GUI,
data etc.) which calls to this object when the acquire data button is pressed.
"""

import algorithm
import Spectrum
from common import c0
from common import convert_to_frequency_spectrum

from PyQt4 import QtGui, QtCore, uic
import numpy as np
import random


compliments = (
    "You look great today!",
    "Your mathematical expertise is astonishing!",
    "I like your style of doing science!",
    "Good work!",
    "Your enthusiasm is contagious!",
    "Keep up the good work!")

class Calibration(QtGui.QWizard):

    def __init__(self, spectrometer, GDD=0, glass_length=0):

        self.spectrometer = spectrometer

        # tau/omega are public containing the correct values (in seconds/Hz)
        self.tau = None
        self.omega = None

        # self.dispersion = 220e-15  # TODO: should calibrate this as well
        self.GDD = GDD  # s^2/m
        self.glass_length = glass_length  # m
        self.unit = None

        # original spectra (for plotting, arbitrary SI-unit)
        self.calib_spec = None
        self.fun_spec = None
        self.omega_spec1 = None
        self.omega_spec2 = None
        self.spectra = []

        # Calibration Wizard: GUI setup
        QtGui.QWizard.__init__(self)
        uic.loadUi("gui/calibration_wizard.ui", self)

        self.page_units.validatePage = self.validate_page_units

        self.page_calib_spec.validatePage = self.validate_spec_page
        self.label_calib_spec_file_error.setVisible(False)
        self.radioButton_calib_spec_file.toggled.connect(self.toggle_file_selection)
        self.pushButton_calib_spec_file.clicked.connect(self.choose_file)

        self.page_fun_spec.validatePage = self.validate_spec_page
        self.label_fun_spec_file_error.setVisible(False)
        self.radioButton_fun_spec_file.toggled.connect(self.toggle_file_selection)
        self.pushButton_fun_spec_file.clicked.connect(self.choose_file)

        self.page_omega1_spec.validatePage = self.validate_spec_page
        self.label_omega1_spec_file_error.setVisible(False)
        self.radioButton_omega1_spec_file.toggled.connect(self.toggle_file_selection)
        self.pushButton_omega1_spec_file.clicked.connect(self.choose_file)

        self.page_omega2_spec.validatePage = self.validate_spec_page
        self.label_omega2_spec_file_error.setVisible(False)
        self.radioButton_omega2_spec_file.toggled.connect(self.toggle_file_selection)
        self.pushButton_omega2_spec_file.clicked.connect(self.choose_file)

        self.page_compliment.initializePage = self.initialize_page_compliment
        self.page_compliment.validatePage = self.validate_page_compliment

    @property
    def dispersion(self):
        return self.GDD*self.glass_length

    def validate_page_units(self):
        if self.radioButton_unit_nm.isChecked():
            self.unit = "nm"
        elif self.radioButton_unit_Hz.isChecked():
            self.unit = "Hz"
        elif self.radioButton_unit_m.isChecked():
            self.unit = "m"
        else:
            return False
        return True

    def validate_spec_page(self):
        page = self.currentPage()

        file_line = self.get_file_line(page)
        file_name = file_line.text()

        if self.get_file_radioButton(page).isChecked():
            # file selection checked
            error_text = self.get_error_text(page)
            try:
                error_text.setVisible(False)
                x, y = np.loadtxt(str(file_name), unpack=True)

                self.spectra += [Spectrum.Spectrum([x, y], self.unit)]

            except IOError:
                error_text.setVisible(True)
                return False

        elif self.get_measure_radioButton(page).isChecked():
            # measure checked
            self.spectra += [self.spectrometer.measure(self.unit)]
        else:
            # this case only for optional step with skip checked
            # add emtpy Spectrum
            self.spectra += [Spectrum.Spectrum()]

        return True

    def initialize_page_compliment(self):
        self.label_compliment.setText(
            compliments[random.randint(0, len(compliments)-1)])

    def validate_page_compliment(self):
        self.calib_spec = self.spectra[0]
        self.fun_spec = self.spectra[1]
        self.omega1_spec = self.spectra[2]
        self.omega2_spec = self.spectra[3]

        return True

    def get_page_widget(self, page, index):
        items = [page.layout().itemAt(i) for i in range(page.layout().count())]
        return items[index].widget()

    def get_error_text(self, page):
        return self.get_page_widget(page, -1)

    def get_file_selection(self, page):
        return self.get_page_widget(page, -2)

    def get_file_line(self, page):
        return self.get_file_selection(page).layout().itemAt(0).widget()

    def get_file_pushButton(self, page):
        return self.get_file_selection(page).layout().itemAt(1).widget()

    def get_file_radioButton(self, page):
        return self.get_page_widget(page, -3)

    def get_measure_radioButton(self, page):
        return self.get_page_widget(page, -4)

    def toggle_file_selection(self):

        page = self.currentPage()

        if self.get_file_radioButton(page).isChecked():
            self.get_file_line(page).setEnabled(True)
            self.get_file_pushButton(page).setEnabled(True)
        else:
            self.get_file_line(page).setEnabled(False)
            self.get_file_pushButton(page).setEnabled(False)

    # missing a file handling object?
    def choose_file(self):
        page = self.currentPage()
        file_line = self.get_file_line(page)

        file_dialog = QtGui.QFileDialog()
        file_name = file_dialog.getOpenFileName(
            None, 'Choose file',
            "", "Text, DAT or CSV files (*.txt *.dat *.csv)")
        file_line.setText(str(file_name))

    def acquire_data(self):
        """Start the calibration wizard"""
        self.restart()
        self.exec_()

    def calibrate(self):
        """This function should be in the prospective calibration-tab module"""
        return algorithm.calibration(
            self.calib_spec.spec_Hz,
            self.dispersion,
            self.fun_spec.spec_Hz)
