# -*- coding: utf-8 -*-
"""
Contains the algorithms for calibration and reconstruction for a simple SPIDER.
"""

from common import get_fwhm, hypergaussian
import numpy as np
import numpy.fft as fft
from scipy.ndimage.interpolation import shift as interpolation_shift
from matplotlib import pyplot as plt
import peakutils
import peakutils.plot
import scipy.integrate
import scipy


def wrap(x):
    """Opposite of numpy's unwrap()"""

    return (x + np.pi) % (2*np.pi) - np.pi


def determine_peaks(x, y, thresh=0.2):

    indices = peakutils.indexes(y, thres=thresh)

    # does not work, some bug suspected :(
    # peaks = peakutils.interpolate(x, y, ind=indices)
    peaks = x[indices]

    if(len(peaks) < 2):
        raise ValueError("Found less than 2 peaks: check your spectrum! "
                         "There should be several interference fringes.")
    return peaks, indices


def calibration(calib_spec, disp=None, fun_spec=None,
                om1_spec=None, om2_spec=None):

    """
    Calculate fringe spacing tau and frequency shift omega for

    :param: frequency-spectrum _in Hz_ of two identical, but mutually delayed
            pulses
    """

    if disp is None:
        raise NotImplementedError("Dispersion must be given, only way to calculate omega ;(")
        return

    x = calib_spec[0]
    y = calib_spec[1]

    peaks, peaks_idx = determine_peaks(x, y)
    peaks.sort()
    peaks_idx.sort()

    taus = [1/(peaks[i] - peaks[i-1]) for i in range(1, len(peaks))]
    tau = np.mean(taus)
    omega = tau/disp

    peaks, peaks_idx = determine_peaks(x, y, thresh=0.1)
    return (tau, omega, [peaks[0], peaks[-1]], [peaks_idx[0], peaks_idx[-1]])


def reconstruct_magnitude(tau,
                          omega,
                          fshift,
                          spec_limits,
                          spec_limits_idx,
                          meas_spec):
    x = meas_spec[0]
    dx = x[3] - x[2]
    y = meas_spec[1]

    t = fft.fftfreq(len(x), dx)
    signal_t = fft.fft(y)

    window_ac = hypergaussian(t, 40, tau, offset=tau).astype(complex)
    window_dc = hypergaussian(t, 40, tau,).astype(complex)

    ac_part_t = signal_t*window_ac
    ac_part_f = fft.ifft(ac_part_t)

    dc_part_t = signal_t*window_dc
    dc_part_f = fft.ifft(dc_part_t)

    # Es ... E _s_hifted by omega, E(w - omega)
    # Es_E ... Es + E
    Es_E_magnitude = np.sqrt(np.abs(dc_part_f) + 2*np.abs(ac_part_f))
    Ess_Es_magnitude = np.roll(Es_E_magnitude, -int(omega/dx))

    E_f_magnitude = 1/2./omega*scipy.integrate.cumtrapz(
        (Ess_Es_magnitude - Es_E_magnitude),
        x,
        initial=0)

    return E_f_magnitude


def reconstruct_phase(tau,
                      omega,
                      fshift,
                      spec_limits,
                      spec_limits_idx,
                      meas_spec,
                      calib_spec):

    low = spec_limits[0]
    up = spec_limits[1]
    low_idx = spec_limits_idx[0]
    up_idx = spec_limits_idx[1]

    x = meas_spec[0]
    dx = x[2] - x[1]
    y = meas_spec[1]

    t = fft.fftfreq(len(x), dx)
    signal_t = fft.fft(y)

    window_ac = hypergaussian(t, 40, tau, offset=tau).astype(complex)

    ac_part_t = signal_t*window_ac
    ac_part_f = fft.ifft(ac_part_t)  # not normalised

    phase_diff = np.unwrap(np.angle(ac_part_f))

    # subtract linear phase wtau
    calib_spec_t = fft.fft(calib_spec[1])
    window_ac = hypergaussian(t, 40, tau, offset=tau).astype(complex)
    ac_part_t = window_ac*calib_spec_t
    ac_part_f = fft.ifft(ac_part_t)
    phase_correction = np.unwrap(np.angle(ac_part_f))
    phase_diff -= phase_correction

    # arbitrarily set phase to zero at center frequency
    f_phase_ref = np.mean([low, up])
    f_phase_ref_idx = (np.abs(x - f_phase_ref)).argmin()
    phase_diff -= phase_diff[f_phase_ref_idx]

    phase = scipy.integrate.cumtrapz(phase_diff, dx=dx*2.*np.pi, initial=0)/omega/2./np.pi

    phase -= phase[f_phase_ref_idx]
    phase = wrap(phase)
    return phase


def reconstruction(tau,
                   omega,
                   fshift,
                   spec_limits,
                   spec_limits_idx,
                   meas_spec,
                   calib_spec,
                   fund_spec=None):

    """
    Calls reconstruction procedures for phase and magnitude and returns
    collected results.

    :param: time shift between interfering pulses
    :param: spectral shear between interfering pulses
    :param: frequency shift due to nonlinear medium
    :param: tupel of border-frequencies defining interesting part of spectrum
    :param: tupel of indices of border-frequencies
    :param: measuerd spectrum of delayed and sheared pulses
    :param: spectrum without shear (obtained during calibration)
    :param: optional, spectrum without second pulse and shear for magnitude
    reconstruction
    """
    x = meas_spec[0]
    dx = meas_spec[0][3] - meas_spec[0][2]

    # shift upconverted spectra down
    # for now negelct interpolation, shouldn't make much of difference
    meas_spec[1] = np.roll(meas_spec[1], -int(fshift/dx))
    calib_spec[1] = np.roll(calib_spec[1], -int(fshift/dx))
    spec_limits[0] -= fshift
    spec_limits[1] -= fshift
    spec_limits_idx[0] -= int(fshift/dx)
    spec_limits_idx[1] -= int(fshift/dx)

    phase = reconstruct_phase(tau,
                              omega,
                              fshift,
                              spec_limits,
                              spec_limits_idx,
                              meas_spec,
                              calib_spec)

    if(fund_spec is not None):

        tmp_dx = fund_spec[0][3] - fund_spec[0][2]
        if tmp_dx > dx:
            f = scipy.interpolate.interp1d(fund_spec[0],
                                           fund_spec[1],
                                           kind='cubic')
            magnitude = np.sqrt(np.abs(f(x)))

        elif tmp_dx < dx:
            f = scipy.interpolate.interp1d(meas_spec[0],
                                           meas_spec[1],
                                           kind='cubic')
            g = scipy.interpolate.interp1d(meas_spec[0],
                                           phase,
                                           kind='cubic')
            x = fund_spec[0]
            dx = tmp_dx

            magnitude = fund_spec[1]
            phase = g(x)

        else:
            magnitude = np.sqrt(np.abs(fund_spec[1]))

    else:
        magnitude = reconstruct_magnitude(tau,
                                          omega,
                                          fshift,
                                          spec_limits,
                                          spec_limits_idx,
                                          meas_spec)
    t = fft.fftfreq(len(x), dx)
    E_f = magnitude * np.exp(1j*phase)
    E_t = fft.ifft(E_f)

    return (t, E_t/max(abs(E_t)), x, E_f/max(abs(E_f)))
