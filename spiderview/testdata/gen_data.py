# -*- coding: utf-8 -*-
"""Generates test data to see if reconstruction algorithm works correctly"""

from matplotlib import pyplot as plt
import numpy as np
from numpy.fft import fft, ifft, fftfreq, fftshift, ifftshift
from numpy import linspace, arange
from numpy import sqrt, exp, log, pi, real, imag, angle
import csv


def smart_limits(data):

    """
    Finds the optimal plotting range for the x-axis.

    Often a spectrum contains mainly zeros and only narrow band is non-zero.
    This function spares the user of manually adjusting the x-axis to the
    interesting range.

    :param: xy-data in standard order (*not* FFT order)
    :return: tupel x-axis indices encapsulating range"""

    threshold = np.max(np.abs(data[1]))*0.005

    for i in range(len(data[1])):
        if np.abs(data[1][i]) > threshold:
            left = data[0][i]
            break

    for i in range(len(data[1])):
        if np.abs(data[1][-i-1]) > threshold:
            right = data[0][-i-1]
            break

    return (left, right)


def nextpow2(x):
    return np.round(np.math.log(x, 2))


def triangle(x, b):
    x = np.asarray(x)
    ret = []
    for val in x:
        if(abs(val) < b):
            if(val < 0.0):
                ret.append((val+b)/b)
            else:
                ret.append((-val+b)/b)
        else:
            ret.append(0.0)
    return np.array(ret)


# GRID
fsample = 10e15  # Hz
tsample = 10e3

t_lin = arange(-2**(nextpow2(tsample)+1), 2**(nextpow2(tsample)+1))/fsample
t = fftshift(t_lin)
f = fftfreq(len(t), 1/fsample)

c = 299792458  # m/s
lam = 790e-9  # m
f0 = c/lam  # Hz
print(f0)
w0 = 2*pi*f0

fwhm = 28e-15  # s
sigma = fwhm/2/sqrt(log(2))
envelope = exp(-t**2/sigma**2)

# INPUT PULSE
pulse_t = sqrt(envelope)*exp(1j*w0*t)
# pulse_t = np.roll(pulse_t, 60)  # linear phase
pulse_f = np.fft.fft(pulse_t)
# theta = pi * triangle((f-f0), 1/fwhm/2)  # triangular phase
theta = pi * (
    2*((f - f0)/f0)**1 -
    7*((f - f0)/f0)**2 +
    10*((f - f0)/f0)**3)  # phase
plt.plot(f, theta)
#plt.show()  # DEBUG
pulse_f *= exp(1j * theta)

if(1):
    plt.subplot(211)
    plt.plot(c/f, abs(pulse_f))
    #plt.xlim([3e14, 5e14])
    plt.subplot(212)
    plt.plot(f, angle(pulse_f))
    plt.xlim([3e14, 5e14])
    plt.grid()
    plt.show()


GVD = 220*1e-27  # s²/m
GV = 1.5e-12     # s /m
thickness = 0.1  # m
GDD = GVD*thickness
loss = 0.8
delay = 400e-15

# ALTERED PULSES
delayed_f = loss * pulse_f * exp(-1j * delay * 2*pi*f)
chirped_f = pulse_f * exp(-1j * thickness * (GV * 2*pi*(f-f0)
                                             + GVD/2 * (2*pi*(f-f0))**2))
delayed_t = (ifft(delayed_f))
chirped_t = (ifft(chirped_f))

if(0):
    plt.plot(t_lin, delayed_t)
    plt.plot(t, chirped_t)
    plt.show()

# COMBINED PULSES
calib_pulse_t = ((pulse_t) + delayed_t)**2
omega1_pulse_t = (pulse_t)*chirped_t
omega2_pulse_t = delayed_t*chirped_t
sum_pulse_t = ((pulse_t) + delayed_t)*chirped_t

if(0):
    plt.subplot(311)
    plt.plot(t, calib_pulse_t)
    plt.subplot(312)
    plt.plot(t, omega1_pulse_t, t, omega2_pulse_t)
    plt.subplot(313)
    plt.plot(t, sum_pulse_t, t, chirped_t)
    plt.show()

# SPECTRUM OF COMBINED PULSES
pulse_f = pulse_f
calib_pulse_f = fft(calib_pulse_t)
omega1_pulse_f = fft(omega1_pulse_t)
omega2_pulse_f = fft(omega2_pulse_t)
sum_pulse_f = fft(sum_pulse_t)
# f = arange(0, len(t))*fsample/len(t)

if(0):
    plt.subplot(311)
    plt.plot(f, calib_pulse_f)
    plt.subplot(312)
    plt.plot(f, omega1_pulse_f, f, omega2_pulse_f)
    plt.subplot(313)
    plt.plot(sum_pulse_f)
    plt.show()

# NORMED INTENSITY SPECTRA
pulse_i = abs(pulse_f)**2/max(abs(pulse_f))**2
calib_pulse_i = abs(calib_pulse_f)**2/max(abs(calib_pulse_f))**2
omega1_pulse_i = abs(omega1_pulse_f)**2/max(abs(omega1_pulse_f))**2
omega2_pulse_i = abs(omega2_pulse_f)**2/max(abs(omega2_pulse_f))**2
sum_pulse_i = abs(sum_pulse_f)**2/max(abs(sum_pulse_f))**2

if(1):
    plt.subplot(311)
    plt.plot(c/f, pulse_i)
    plt.xlim(smart_limits([c/f, pulse_i]))
    plt.subplot(312)
    plt.plot(c/f, calib_pulse_i)
    plt.xlim(smart_limits([c/f, calib_pulse_i]))
    plt.plot(c/f, sum_pulse_i)
    plt.xlim(smart_limits([c/f, sum_pulse_i]))
    plt.subplot(313)
    plt.plot(c/f, omega1_pulse_i, c/f, omega2_pulse_i)
    plt.xlim(smart_limits([c/f, omega1_pulse_i]))
    plt.show()


# WRITE TO FILES
x_data = c/f

with open('pulse_t.txt', 'w') as file:
    writer = csv.writer(file, delimiter=' ')
    writer.writerows(zip(t,
                         pulse_t))

with open('pulse_t_i.txt', 'w') as file:
    writer = csv.writer(file, delimiter=' ')
    writer.writerows(zip(t,
                         abs(pulse_t)**2))

with open('spec.txt', 'w') as file:
    writer = csv.writer(file, delimiter=' ')
    writer.writerows(zip(x_data,
                         pulse_f/max(np.abs(pulse_f))))

with open('fundamental.txt', 'w') as file:
    writer = csv.writer(file, delimiter=' ')
    writer.writerows(zip(x_data, pulse_i))

with open('calibration.txt', 'w') as file:
    writer = csv.writer(file, delimiter=' ')
    writer.writerows(zip(x_data, calib_pulse_i))

with open('omega1.txt', 'w') as file:
    writer = csv.writer(file, delimiter=' ')
    writer.writerows(zip(x_data, omega1_pulse_i))

with open('omega2.txt', 'w') as file:
    writer = csv.writer(file, delimiter=' ')
    writer.writerows(zip(x_data, omega2_pulse_i))

with open('sumpulse.txt', 'w') as file:
    writer = csv.writer(file, delimiter=' ')
    writer.writerows(zip(x_data, sum_pulse_i))

# compare to Kirchner data
if(1):
    plt.subplot(511)
    # plt.plot(c/f*1e9, pulse_i)
    x, y = np.loadtxt('fundamental.txt', unpack=True)
    plt.plot(x*1e9, y)
    plt.axis([600, 1200, 0, 1])

    x, y = np.loadtxt('kirchner/sim_fundamental.txt', unpack=True)
    plt.plot(x, y)

    plt.subplot(512)
    # plt.plot(c/f*1e9, calib_pulse_i)
    x, y = np.loadtxt('calibration.txt', unpack=True)
    plt.plot(x*1e9, y)
    plt.axis([300, 1200, 0, 1])

    x, y = np.loadtxt('kirchner/sim_calibration_spec.txt', unpack=True)
    plt.plot(x, y)

    plt.subplot(513)
    # plt.plot(c/f*1e9, omega1_pulse_i)
    x, y = np.loadtxt('omega1.txt', unpack=True)
    plt.plot(x*1e9, y)
    plt.axis([300, 1200, 0, 1])

    x, y = np.loadtxt('kirchner/sim_om_calib_first.txt', unpack=True)
    plt.plot(x, y)

    plt.subplot(514)
    # plt.plot(c/f*1e9, omega2_pulse_i)
    x, y = np.loadtxt('omega2.txt', unpack=True)
    plt.plot(x*1e9, y)
    plt.axis([300, 1200, 0, 1])

    x, y = np.loadtxt('kirchner/sim_om_calib_blue.txt', unpack=True)
    plt.plot(x, y)

    plt.subplot(515)
    # plt.plot(c/f*1e9, sum_pulse_i)
    x, y = np.loadtxt('sumpulse.txt', unpack=True)
    plt.plot(x*1e9, y)
    plt.axis([300, 1200, 0, 1])

    x, y = np.loadtxt('kirchner/sim_measurement_spec.txt', unpack=True)
    plt.plot(x, y)

#    plt.show()
